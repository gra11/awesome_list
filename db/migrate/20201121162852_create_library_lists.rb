class CreateLibraryLists < ActiveRecord::Migration[6.0]
  def change
    create_table :library_lists do |t|
      t.jsonb :data

      t.timestamps
    end
  end
end
