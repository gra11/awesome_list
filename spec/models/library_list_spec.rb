require 'rails_helper'

describe LibraryList, type: :model do
  describe '.by_language' do
    before { FactoryBot.create :library_list, data: { language: 'ruby' } }

    it 'includes proper amount of records' do
      expect(LibraryList.by_language('ruby').count).to eq(1)
    end

    it 'don\'t includes proper amount of records' do
      expect(LibraryList.by_language('phyton').count).to eq(0)
    end
  end

  describe '.by_language_and_category' do
    before { FactoryBot.create :library_list, data: { language: 'ruby', category: 'cms' } }

    it 'includes proper amount of records' do
      expect(LibraryList.by_language_and_category('ruby', 'cms').count).to eq(1)
    end

    it 'don\'t includes proper amount of records when first argument match' do
      expect(LibraryList.by_language_and_category('ruby', 'bundlers').count).to eq(0)
    end

    it 'don\'t includes proper amount of records when second argument match' do
      expect(LibraryList.by_language_and_category('phyton', 'cms').count).to eq(0)
    end
  end

  describe '.select_distinct_language' do
    before do
      FactoryBot.create :library_list, data: { language: 'ruby', category: 'cms' }
      FactoryBot.create :library_list, data: { language: 'ruby', category: 'bundlers' }
    end

    let(:query_result) { JSON.parse(LibraryList.select_distinct_language.to_json) }

    it 'includes proper amount of records' do
      expect(query_result.count).to eq(1)
    end

    it 'includes proper item' do
      expect(query_result.first['item']).to eq('ruby')
    end

    it 'includes more items amount' do
      FactoryBot.create :library_list, data: { language: 'phyton', category: 'bundlers' }

      expect(query_result.count).to eq(2)
    end

    it 'includes different items' do
      FactoryBot.create :library_list, data: { language: 'phyton', category: 'bundlers' }

      parsed_query_result = query_result.map { |i| i['item'] }

      expect(parsed_query_result).to include('ruby')
      expect(parsed_query_result).to include('phyton')
    end
  end

  describe '.select_distinct_language_and_category' do
    before do
      FactoryBot.create :library_list, data: { language: 'ruby', category: 'cms', repo: 'fae' }
      FactoryBot.create :library_list, data: { language: 'ruby', category: 'cms', repo: 'lokomotive' }
    end

    let(:query_result) { JSON.parse(LibraryList.select_distinct_language_and_category.to_json) }

    it 'includes proper amount of records' do
      expect(query_result.count).to eq(1)
    end

    it 'includes proper item' do
      expect(query_result.first['item']).to eq('cms')
    end

    it 'includes more items amount' do
      FactoryBot.create :library_list, data: { language: 'ruby', category: 'bundlers' }

      expect(query_result.count).to eq(2)
    end

    it 'includes different items' do
      FactoryBot.create :library_list, data: { language: 'ruby', category: 'bundlers' }

      parsed_query_result = query_result.map { |i| i['item'] }

      expect(parsed_query_result).to include('cms')
      expect(parsed_query_result).to include('bundlers')
    end
  end

  describe '.select_repo_attributes' do
    before do
      FactoryBot.create(
        :library_list,
          data: {
            language: 'ruby',
            category: 'cms',
            repo: 'fae',
            fork_count: 111,
            star_count: 321,
            pushed_at: '10/10/2021'
          }
      )
    end

    let(:query_result) { JSON.parse(LibraryList.select_repo_attributes.to_json) }

    it 'includes proper item' do
      expect(query_result.first['name']).to eq('fae')
      expect(query_result.first['fork_count']).to eq('111')
      expect(query_result.first['star_count']).to eq('321')
      expect(query_result.first['pushed_at']).to eq('10/10/2021')
    end
  end
end