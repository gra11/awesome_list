FactoryBot.define do
  factory :library_list do
    data { { language: 'ruby' } }
  end
end