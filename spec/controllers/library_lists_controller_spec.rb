require 'rails_helper'

describe LibraryListsController, type: :controller do
  describe 'GET index' do
    before { FactoryBot.create :library_list, data: { language: 'ruby' } }

    it 'successfully handle index request' do
      get :index

      expect(response.status).to eq(200)
      expect(response).to render_template(:index)
    end
  end

  describe 'GET new' do
    it 'successfully handle new request' do
      get :new

      expect(response.status).to eq(200)
      expect(response).to render_template(:new)
      expect(assigns(:library_list)).to be_a_new(LibraryList)
    end
  end

  describe 'POST create' do
    let(:library_list_params) { { language: 'ruby', category: 'cms', repo: 'loko' } }
    let(:stubbed_data) { OpenStruct.new(stargazer_count: '111', fork_count: '222', pushed_at: '10/10/2020') }

    it 'creates a new LibraryList' do
      allow(LibraryLists::GetRepoQuery).to receive_message_chain('find.repository').and_return(stubbed_data)

      expect {
        post :create, params: { library_list: library_list_params }
      }.to change(LibraryList, :count).by(1)
    end
  end
end