Rails.application.routes.draw do
  root to: 'library_lists#index'

  get '/', to: 'library_lists#index'
  get '/new', to: 'library_lists#new'
  post '/new', to: 'library_lists#create'
  get '/*language', to: 'library_lists#index', as: 'language'
end
