module LibraryLists
  class CreateItemService
    def initialize(params)
      @params = params
    end

    def perform
      build_data_for_saving
    end

    private

    attr_reader :params

    def build_data_for_saving
      {
        data: {
          language: params[:language].downcase,
          category: params[:category].downcase,
          repo: params[:repo].gsub(' ', '').split('/').last,
          star_count: get_data.stargazer_count,
          fork_count: get_data.fork_count,
          pushed_at: get_data.pushed_at.to_date.strftime('%d/%m/%Y')
        }
      }
    end

    def get_data
      @_data ||= LibraryLists::GetRepoQuery.find(repo_name_and_user[0], repo_name_and_user[1]).repository
    end

    def repo_name_and_user
      @_repo_name_and_user ||= params[:repo].gsub(' ', '').split('/')
    end
  end
end