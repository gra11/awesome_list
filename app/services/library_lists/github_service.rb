require "graphql/client"
require "graphql/client/http"

module LibraryLists
  module GithubService
    GITHUB_ACCESS_TOKEN = ENV['GITHUB_ACCESS_TOKEN']
    URL = 'https://api.github.com/graphql'

    HTTP = GraphQL::Client::HTTP.new(URL) do
      def headers(context)
        {
          "Authorization": "Bearer #{GITHUB_ACCESS_TOKEN}",
          "User-Agent": 'Ruby'
        }
      end
    end

    Schema = GraphQL::Client.load_schema(HTTP)
    Client = GraphQL::Client.new(schema: Schema, execute: HTTP)
  end
end
