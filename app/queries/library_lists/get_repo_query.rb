module LibraryLists
  class GetRepoQuery
    RepoQuery = GithubService::Client.parse <<-'GRAPHQL'
      query($user: String!, $repo: String!) {
        repository(owner: $user, name: $repo) {
          stargazerCount
          forkCount
          isFork
          pushedAt
        }
      }
    GRAPHQL

    def self.find(user, repo)
      LibraryLists::GithubService::Client.query(RepoQuery, variables: { user: user, repo: repo }).data
    end
  end
end