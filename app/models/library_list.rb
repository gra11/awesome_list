class LibraryList < ApplicationRecord
  LANGUAGE      = 'language'
  CATEGORY      = 'category'
  REPO          = 'repo'
  URL_SEPARATOR = '/'

  scope :by_language, -> (language) { where("data -> 'language' ? :language", language: language) }
  scope :by_language_and_category, -> (language, category) do
    where("data -> 'language' ? :language AND data -> 'category' ? :category", language: language, category: category)
  end
  scope :select_distinct_language, -> { select("data -> 'language' as item").distinct }
  scope :select_distinct_language_and_category, -> do
    select("data -> 'category' as item", "data -> 'language' as language").distinct
  end
  scope :select_repo_attributes, -> do
    select(
        "data -> 'repo' as name",
        "data -> 'star_count' as star_count",
        "data -> 'fork_count' as fork_count",
        "data -> 'pushed_at' as pushed_at"
    )
  end
end
