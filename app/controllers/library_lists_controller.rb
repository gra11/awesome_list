class LibraryListsController < ApplicationController
  before_action :set_library_list_type, only: [:index, :create]
  before_action :set_library_list, only: :index

  def index
    render :index, locals: { library_list_type: @library_list_type }
  end

  def new
    @library_list = LibraryList.new
  end

  def create
    @library_list = LibraryList.new create_item_service.perform

    if @library_list.save
      set_library_list

      render :index, locals: { library_list_type: @library_list_type }
    else
      render :new
    end
  end

  private

  def permitted_params
    params.require(:library_list).permit(:language, :category, :repo)
  end

  def create_item_service
    LibraryLists::CreateItemService.new(permitted_params)
  end

  def set_library_list
    @library_lists = params[:language].present? ? find_by_language_path : LibraryList.select_distinct_language
  end

  def find_by_language_path
    if params[:language].include?(LibraryList::URL_SEPARATOR)
      LibraryList.by_language_and_category(library_path.first, library_path.last).select_repo_attributes
    else
      LibraryList.by_language(library_path.first).select_distinct_language_and_category
    end
  end

  def library_path
    params[:language].split(LibraryList::URL_SEPARATOR) if params[:language].present?
  end

  def set_library_list_type
    @library_list_type = params[:language].present? ? set_repo_or_category_type : LibraryList::LANGUAGE
  end

  def set_repo_or_category_type
    if params[:language].include?(LibraryList::URL_SEPARATOR)
      LibraryList::REPO
    else
      LibraryList::CATEGORY
    end
  end
end